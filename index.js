// const request = require('request');
const Telegram = require('node-telegram-bot-api');
const config = require('./config');
const fs = require('fs');
const util = require('util');
const log_file = fs.createWriteStream(__dirname + '/logs.log', {flags : 'a'});

const bot = new Telegram(config.botToken, {polling: true});

const admins = config.admins;
const targetGroup = config.targetGroup;

let noticed = false;
const hours = [1,4,7,10,13,16,19,22];

function stdControl(message) {
    process.stdout.clearLine();  // clear current text
    process.stdout.cursorTo(0);  // move cursor to beginning of line
    process.stdout.write(message);
}

function log(msg) {
    console.log(`\n${new Date().toString().slice(16, 24)} — ${msg}`);
    log_file.write(`\r\n${new Date().toString().slice(16, 24)}: ${util.format(msg)}`);
}

function ValidURL(str) {
    regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    return regexp.test(str);
}

setInterval(() => {
    let current_min = new Date().getMinutes();
    let current_hours = new Date().getHours();


    if(!noticed) {
        if(hours.includes(current_hours) && current_min === 30) {
            if(config.targetGroup < 0) {
                bot.sendMessage(targetGroup, 'A new day has come!');
            } else {
                log(`specify the group number in config.js file`)
            }
            noticed = true;
            log(`Users are notified`)
        }
    } else {
        if(current_min !== 30) {
            noticed = false
        }
    }
    stdControl(new Date().toString().slice(16, 24))
}, 1000);

bot.on('message', (msg) => {
    if(msg.text) {
        let msgText = msg.text;
        let userId = msg.from.id;
        let fromChatId = msg.chat.id;
        let fromAdmin = false;

        if(targetGroup === fromChatId) { // check if command was was recieved from specific chat

            fromAdmin = admins.includes(userId); // true or false (checking if admins array includes current user id)

            if(msgText === '/checkAdmin') {
                log(`Target group /checkAdmin msg from user ${userId}`);
                let answer = fromAdmin ? 'Admin' : 'Holop';
                bot.sendMessage(fromChatId, answer, {reply_to_message_id: msg.message_id})
            }

            if(fromAdmin && msgText === '/groupId') {
                log(`Target group /groupId msg from user ${userId}`);
                bot.sendMessage(fromChatId, `Your chat id is ${fromChatId}`)
            }

            if(msgText === '/plan') {
                log(`Target group /plan request from user ${userId}`);
                let imgLink;
                fs.readFile('plan.json', 'utf-8', (err, data) => {
                    if(err) {
                        throw new Error(err)
                    } else {
                        imgLink = JSON.parse(data).url
                        bot.sendPhoto(targetGroup, imgLink)
                    }
                });
                // let pic_stream = request.get(imgLink).on('error', (err) => { log(`Image link error: ${err}`) });
                // bot.sendPhoto(targetGroup, pic_stream);
            }
        } else if(admins.includes(fromChatId)) { // check if command was was recieved in private chat from admin
            if(msgText.includes('http://') || msgText.includes('https://')) {
                log(`Plan link from admin ${fromChatId}`);
                if(ValidURL(msgText)) {
                    log(`Plan image link from admin ${fromChatId}`);
                    let plan = {};
                    plan.url = msgText;

                    fs.writeFile('plan.json', JSON.stringify(plan, null, 4), () => {
                        bot.sendMessage(fromChatId, `Okay. New plan link: ${msgText}`)
                    });
                } else {
                    bot.sendMessage(fromChatId, 'This URL is not valid')
                }
            }
        } else {
            if(fromChatId < 0) {
                log(`Message from unknown chat ${fromChatId}`);
            } else {
                log(`Message from unknown holop ${fromChatId}`);
            }
        }
    }
});

bot.on('polling_error', (error) => {
    log(`polling_error: ${error.message}`);  // => 'EFATAL'
});
