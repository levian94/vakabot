const path = require('path');
require('dotenv').config({
    path: path.join(__dirname, '/.env'),
});

module.exports = {
    botToken: process.env.BOT_TOKEN,
    admins: JSON.parse(`[${process.env.ADMINS}]`),
    targetGroup: Number(process.env.GROUP_ID)
};
